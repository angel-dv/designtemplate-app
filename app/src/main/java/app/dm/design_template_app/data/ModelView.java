package app.dm.design_template_app.data;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

import app.dm.design_template_app.model.Greeting;
import app.dm.design_template_app.model.Menssage;

public class ModelView extends AndroidViewModel {

    private Repository repository;
    private LiveData<List<Menssage>> arrayMenssage;
    private LiveData<List<Greeting>> allGreeting;



    public ModelView(@NonNull Application application) {
        super(application);
        repository = new Repository();
        arrayMenssage = repository.getAllMenssage();
        allGreeting = repository.getAllGreeting();
    }

    public LiveData<List<Menssage>> getAllMenssage() {
        return arrayMenssage;
    }

    public void insertMenssage(String name, String menssage){
        repository.createMenssage(name, menssage);
    }

    public LiveData<List<Greeting>> getAllGreeting(){
        return allGreeting;
    }

    public void insertGreeting(String name, String cheers){
        repository.createGretting(name, cheers);
    }
}
