package app.dm.design_template_app.data;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import app.dm.design_template_app.model.Greeting;
import app.dm.design_template_app.model.Menssage;

public class Repository {
    private MutableLiveData<List<Menssage>> allMenssage;
    private MutableLiveData<List<Greeting>> allGreeting;
    private ArrayList<Menssage> menssageArrayList;

    public Repository() {
        allMenssage = getAllMenssage();
        allGreeting = getAllGreeting();
    }

    public MutableLiveData<List<Menssage>> getAllMenssage() {
        if (allMenssage == null) {
            allMenssage = new MutableLiveData<>();
        }

        Menssage m = new Menssage();
        m.setNameUser("Angel");
        m.setMenssage("Que onda");

        menssageArrayList = new ArrayList<>();

        for (int i = 0; i < 4; i++) {
            menssageArrayList.add(m);
        }

        allMenssage.setValue(menssageArrayList);

        return allMenssage;
    }

    public void createMenssage(String name, String menssage) {
        allMenssage.setValue(Collections.singletonList(new Menssage(name, menssage)));

        List<Menssage> listaClonada = new ArrayList<>();
        listaClonada.addAll(menssageArrayList);

        for(int i=0; i < allMenssage.getValue().size(); i++) {
            listaClonada.add(new Menssage(allMenssage.getValue().get(i)));
        }
        allMenssage.setValue(listaClonada);
    }

    public MutableLiveData<List<Greeting>> getAllGreeting(){
        if (allGreeting == null){
            allGreeting = new MutableLiveData<>();
        }
        return allGreeting;
    }

    public void createGretting(String name, String cheers){
        allGreeting.setValue(Collections.singletonList(new Greeting(name, cheers)));
    }

}
