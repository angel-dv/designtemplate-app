package app.dm.design_template_app.model;

public class Greeting {
    private int id;
    private String name;
    private String cheers;

    public Greeting(){}

    public Greeting(String name, String cheers) {
        this.name = name;
        this.cheers = cheers;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCheers() {
        return cheers;
    }

    public void setCheers(String cheers) {
        this.cheers = cheers;
    }
}
