package app.dm.design_template_app.model;

public class Book {

    private String title, description, author, inmgUrl;
    private int pages, review;
    private float rating;
    private int drawableResource;

    public Book (){

    }

    public Book(int drawableResource) {
        this.drawableResource = drawableResource;
    }

    public Book(String title, String description, String author, String inmgUrl, int pages, int review, float rating, int drawableResource) {
        this.title = title;
        this.description = description;
        this.author = author;
        this.inmgUrl = inmgUrl;
        this.pages = pages;
        this.review = review;
        this.rating = rating;
        this.drawableResource = drawableResource;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getInmgUrl() {
        return inmgUrl;
    }

    public void setInmgUrl(String inmgUrl) {
        this.inmgUrl = inmgUrl;
    }

    public int getPages() {
        return pages;
    }

    public void setPages(int pages) {
        this.pages = pages;
    }

    public int getReview() {
        return review;
    }

    public void setReview(int review) {
        this.review = review;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public int getDrawableResource() {
        return drawableResource;
    }

    public void setDrawableResource(int drawableResource) {
        this.drawableResource = drawableResource;
    }
}
