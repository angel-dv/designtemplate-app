package app.dm.design_template_app.model;

public class Menssage {

    private int id;
    private String menssage;
    private String nameUser;

    public Menssage(){

    }

    public Menssage(String nameUser, String menssage) {
        this.menssage = menssage;
        this.nameUser = nameUser;
    }

    public Menssage(Menssage nuevoMenssage){
        this.id = nuevoMenssage.getId();
        this.nameUser = nuevoMenssage.getNameUser();
        this.menssage = nuevoMenssage.getMenssage();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMenssage() {
        return menssage;
    }

    public void setMenssage(String menssage) {
        this.menssage = menssage;
    }

    public String getNameUser() {
        return nameUser;
    }

    public void setNameUser(String nameUser) {
        this.nameUser = nameUser;
    }
}
