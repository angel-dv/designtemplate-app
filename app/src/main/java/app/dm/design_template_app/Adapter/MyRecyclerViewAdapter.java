package app.dm.design_template_app.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import app.dm.design_template_app.R;
import app.dm.design_template_app.model.Menssage;

public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyRecyclerViewAdapter.ViewHolder> {

    private List<Menssage> arrayMenssage;
    private Context ctx;

    public MyRecyclerViewAdapter(Context ctx, List<Menssage> arrayMenssage) {
        this.arrayMenssage = arrayMenssage;
        this.ctx = ctx;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recyclerview, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, int position) {
        Menssage m = arrayMenssage.get(position);

        holder.tvUser.setText(m.getNameUser());
        holder.tvMessage.setText(m.getMenssage());
        holder.ivAvatar.setImageDrawable(ctx.getResources().getDrawable(R.drawable.ic_account_circle, null));
    }

    @Override
    public int getItemCount() {
        if (arrayMenssage != null)
            return arrayMenssage.size();
        else return 0;
    }

    public void setData(List<Menssage> arrayMenssage) {
        this.arrayMenssage = arrayMenssage;
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public final TextView tvUser, tvMessage;
        public final ImageView ivAvatar;

        public ViewHolder(@NonNull View v) {
            super(v);
            ivAvatar = v.findViewById(R.id.imageViewAvatar);
            tvUser = v.findViewById(R.id.textViewUsername);
            tvMessage = v.findViewById(R.id.textViewMessage);
        }
    }
}
