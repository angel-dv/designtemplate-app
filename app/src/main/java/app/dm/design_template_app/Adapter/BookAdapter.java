package app.dm.design_template_app.Adapter;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.bitmap.CenterCrop;
import com.bumptech.glide.load.resource.bitmap.RoundedCorners;

import java.util.List;

import app.dm.design_template_app.R;
import app.dm.design_template_app.model.Book;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.bookviewholder> {

    List<Book> mdata;

    public BookAdapter(List<Book> mdata) {
        this.mdata = mdata;
    }

    @NonNull
    @Override
    public bookviewholder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_book, parent, false);

        return new bookviewholder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull bookviewholder holder, int position) {

        Glide.with(holder.itemView.getContext())
                .load(mdata.get(position).getDrawableResource())
                .transform(new CenterCrop(), new RoundedCorners(16))
                .into(holder.ivbook);


        holder.rbbook.setRating((float) 4.5);
    }

    @Override
    public int getItemCount() {
        return mdata.size();
    }

    public class bookviewholder extends RecyclerView.ViewHolder {

        ImageView ivbook, ivFav;
        TextView tvTitle, tvAuthor, tvPages, tvRate;
        RatingBar rbbook;

        public bookviewholder(@NonNull View v) {
            super(v);

            ivbook = v.findViewById(R.id.imageViewBook);
            ivFav = v.findViewById(R.id.imageViewFavorite);
            tvTitle = v.findViewById(R.id.textViewTitle);
            tvAuthor = v.findViewById(R.id.textViewAutor);
            tvPages = v.findViewById(R.id.textViewPageView);
            tvRate = v.findViewById(R.id.textViewScore);
            rbbook = v.findViewById(R.id.ratingBarBook);

        }
    }
}
