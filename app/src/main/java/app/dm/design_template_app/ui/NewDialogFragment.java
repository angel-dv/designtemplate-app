package app.dm.design_template_app.ui;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProviders;

import app.dm.design_template_app.R;
import app.dm.design_template_app.data.ModelView;

public class NewDialogFragment extends DialogFragment implements View.OnClickListener {

    private ImageView ivClose, ivAvatar;
    private EditText etMensaje;
    private Button btnSave;

    public NewDialogFragment() {

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.FullScreenDialogStyle);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View v = inflater.inflate(R.layout.new_full_dialog, container, false);

        ivClose = v.findViewById(R.id.imageViewClose);
        ivAvatar = v.findViewById(R.id.imageViewDialogAvatar);
        etMensaje = v.findViewById(R.id.editTextMensaje);
        btnSave = v.findViewById(R.id.buttonAgregar);

        ivClose.setOnClickListener(this);
        btnSave.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();

        String mensaje = etMensaje.getText().toString();

        if (id == R.id.buttonAgregar) {
            if (mensaje.isEmpty()) {
                Toast.makeText(getActivity(), "Debe escribir un texto en el mensaje", Toast.LENGTH_SHORT).show();
            } else {
                ModelView modelView = ViewModelProviders
                        .of(getActivity()).get(ModelView.class);
                modelView.insertMenssage(mensaje, "");
                getDialog().dismiss();
            }
        } else if (id == R.id.imageViewClose) {
            if (!mensaje.isEmpty()) {
                showDialogConfirm();
            } else {
                getDialog().dismiss();
            }
        }

    }

    private void showDialogConfirm() {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        builder.setMessage("¿Desea realmente eliminar el mensaje? El mensaje se borrará")
                .setTitle("Cancelar mensaje");

        builder.setPositiveButton("Eliminar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
                getDialog().dismiss();
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });

        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
