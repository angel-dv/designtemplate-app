package app.dm.design_template_app.ui;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import java.util.ArrayList;
import java.util.List;

import app.dm.design_template_app.Adapter.BookAdapter;
import app.dm.design_template_app.R;
import app.dm.design_template_app.model.Book;

public class BooksActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private BookAdapter adapter;
    private List<Book> mdata;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        initViews();
        initmdataBooks();
        setupBookAdapter();

    }

    private void initViews() {
        recyclerView = findViewById(R.id.recyclerViewBooks);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setHasFixedSize(true);
    }

    private void initmdataBooks() {
        mdata = new ArrayList<>();
        mdata.add(new Book(R.drawable.aprendiendo));
        mdata.add(new Book(R.drawable.aprendiendo_2));
        mdata.add(new Book(R.drawable.mente));
        mdata.add(new Book(R.drawable.rico));
        mdata.add(new Book(R.drawable.salvese));
        mdata.add(new Book(R.drawable.shadow));
    }

    private void setupBookAdapter() {
        adapter = new BookAdapter(mdata);
        recyclerView.setAdapter(adapter);
    }
}
