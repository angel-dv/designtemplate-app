package app.dm.design_template_app.ui;

import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import app.dm.design_template_app.R;
import app.dm.design_template_app.data.ModelView;
import app.dm.design_template_app.model.Greeting;
import app.dm.design_template_app.model.Menssage;

public class LikeFragment extends Fragment {

    private EditText etLikeName, etLikeSaludo;
    private TextView tvContent;
    private Button btnClick, btnLookBook;
    private ModelView modelView;

    public LikeFragment() {
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        modelView = ViewModelProviders.of(getActivity()).get(ModelView.class);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_like, container, false);

        etLikeName = v.findViewById(R.id.editTextLikeName);
        etLikeSaludo = v.findViewById(R.id.editTextLikeSaludo);
        tvContent = v.findViewById(R.id.textViewLikeContent);
        btnClick = v.findViewById(R.id.buttonClick);
        btnLookBook = v.findViewById(R.id.buttonLookBook);

        btnLookBook.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), BooksActivity.class);
                startActivity(i);
            }
        });

        btnClick.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = etLikeName.getText().toString();
                String saludo = etLikeSaludo.getText().toString();

                if(!name.isEmpty() && !saludo.isEmpty()){
                    lookContent(name, saludo);
                } else {
                    Toast.makeText(getActivity(), "Datos vacios", Toast.LENGTH_SHORT).show();
                }

            }
        });

        allGreeting();

        return v;
    }

    private void lookContent(String name, String saludo) {
        modelView = ViewModelProviders.of(getActivity()).get(ModelView.class);
        modelView.insertGreeting(name, saludo);

        allGreeting();
    }

    private void allGreeting() {
        modelView.getAllGreeting().observe(getActivity(), new Observer<List<Greeting>>() {
            @Override
            public void onChanged(List<Greeting> greetings) {
                String n = "", g = "";
                for (int i = 0; i < greetings.size(); i++) {
                    n = greetings.get(i).getName();
                    g = greetings.get(i).getCheers();
                }
                if (!n.isEmpty() && !g.isEmpty()){
                    tvContent.setText(String.format("Saludos de: %s \nCon el mensaje de: %s", n, g));
                }
            }
        });
    }

}
