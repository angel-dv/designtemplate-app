package app.dm.design_template_app.ui;

import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import app.dm.design_template_app.model.Menssage;
import app.dm.design_template_app.Adapter.MyRecyclerViewAdapter;
import app.dm.design_template_app.R;
import app.dm.design_template_app.data.ModelView;
import app.dm.design_template_app.ui.utils.Constantes;

public class HomeFragment extends Fragment {
    private int homeTypeNumb = 1;
    private RecyclerView recyclerView;
    private MyRecyclerViewAdapter adapter;
    private List<Menssage> arrayMessage;
    ModelView modelView;

    public static HomeFragment newInstance(int homeType) {
        HomeFragment hf = new HomeFragment();
        Bundle args = new Bundle();
        args.putInt(Constantes.HOME_TYPE, homeType);
        hf.setArguments(args);
        return hf;
    }

    public HomeFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        modelView = ViewModelProviders.of(getActivity()).get(ModelView.class);

        if (getArguments() != null) {
            homeTypeNumb = getArguments().getInt(Constantes.HOME_TYPE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        Context context = v.getContext();
        recyclerView = v.findViewById(R.id.recyclerView);
        arrayMessage = new ArrayList<>();

        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new MyRecyclerViewAdapter(getActivity(), arrayMessage);
        recyclerView.setAdapter(adapter);

        allMenssage();

        String n = "Luis Angel", m = "Buenas tardes";
        insertData(n, m);

        return v;
    }

    private void insertData(String name, String menssage) {
        modelView = ViewModelProviders.of(getActivity()).get(ModelView.class);
        modelView.insertMenssage(name, menssage);
    }

    private void allMenssage() {
        modelView.getAllMenssage().observe(getActivity(), new Observer<List<Menssage>>() {
            @Override
            public void onChanged(List<Menssage> menssages) {
                arrayMessage = menssages;
                adapter.setData(arrayMessage);
            }
        });
    }

}
