package app.dm.design_template_app.ui;

import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import app.dm.design_template_app.R;
import app.dm.design_template_app.ui.utils.Constantes;

public class BottomNavigationActivity extends AppCompatActivity {

    private FloatingActionButton fab;
    private ImageView ivAvatar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_navigation);

        fab = findViewById(R.id.fab);
        ivAvatar = findViewById(R.id.circleImageViewToolbarPhoto);
        ivAvatar.setImageDrawable(getResources().getDrawable(R.drawable.ic_account_circle, null));

        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.fragmentContainer, HomeFragment.newInstance(Constantes.HOME_TYPE_INT))
                .commit();

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NewDialogFragment dialog = new NewDialogFragment();
                dialog.show(getSupportFragmentManager(), "NewDialogFragment");
            }
        });
    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment f = null;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    f = new HomeFragment();
                    fab.show();
                    break;
                case R.id.navigation_like:
                    f = new LikeFragment();
                    fab.hide();
                    break;
                case R.id.navigation_profile:
                    f = new ProfileFragment();
                    fab.hide();
                    break;
            }

            if (f != null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragmentContainer, f)
                        .commit();
                return true;
            }

            return false;
        }
    };
}
